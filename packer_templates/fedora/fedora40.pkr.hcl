variables {
    iso_url = "https://mirrors.rit.edu/fedora/fedora/linux/releases/40/Server/x86_64/iso/Fedora-Server-dvd-x86_64-40-1.14.iso"
    iso_checksum = "md5:18be3f701af64bd6484f3ece06acddf3"
    boot_command = ["<wait><up>e<wait><down><down><end> inst.text inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ks.cfg<F10><wait>"]
}

source "qemu" "fedora40" {
    iso_url = var.iso_url
    iso_checksum = var.iso_checksum
    shutdown_command = "echo 'password' | sudo -S shutdown -P now"
    disk_size = "30000M"
    memory = 5120
    format = "qcow2"
    accelerator = "kvm"
    http_content = {
        "/ks.cfg" = templatefile("${path.root}/fedora40.pkrtpl", {groups = []})
    }
    ssh_username = "vagrant"
    ssh_password = "password"
    ssh_timeout = "50m"
    vm_name = "${source.name}"
    net_device = "virtio-net"
    disk_interface = "virtio"
    boot_wait = "5s"
    boot_command = var.boot_command
}

source "qemu" "fedora40-kde" {
    iso_url = var.iso_url
    iso_checksum = var.iso_checksum
    shutdown_command = "echo 'password' | sudo -S shutdown -P now"
    disk_size = "30000M"
    memory = 5120
    format = "qcow2"
    accelerator = "kvm"
    http_content = {
        "/ks.cfg" = templatefile("${path.root}/fedora40.pkrtpl", {groups = ["kde-desktop"]})
    }
    ssh_username = "vagrant"
    ssh_password = "password"
    ssh_timeout = "50m"
    vm_name = "${source.name}"
    net_device = "virtio-net"
    disk_interface = "virtio"
    boot_wait = "5s"
    boot_command = var.boot_command
}

build {
    sources = ["source.qemu.fedora40", "source.qemu.fedora40-kde"]
    provisioner "shell" {
        expect_disconnect = true
        scripts = [
            "${path.root}/scripts/crypto-policy.sh",
            "${path.root}/scripts/update.sh",
            "${path.root}/../common/reboot.sh",
            "${path.root}/scripts/cleanup.sh",
            "${path.root}/../common/x.sh",
            "${path.root}/../common/vagrant.sh",
            "${path.root}/../common/love.sh",
            "${path.root}/../common/machine-id-and-random-seed.sh",
            "${path.root}/../common/logs-and-cache.sh",
            "${path.root}/../common/minimize.sh"
        ]
    }
    post-processor "vagrant" {
        output = "${source.name}.box"
        vagrantfile_template = "files/Vagrantfile"
    }
}
