variables {
    onyx_iso_url = "osnova_1.iso"
    onyx_iso_checksum = "md5:874eb91d42b4c9bcb008f489a3f94c76"
    strelets_iso_url = "strelets-1.0-28.11.2022.iso"
    strelets_iso_checksum = "md5:5c0a1fe50bd114ba2c91f97c4c0988ee"
    boot_command = [
        "<esc><wait><enter><wait>",
        "install preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/pressed.cfg <wait>",
        "auto ",
        "netcfg/get_hostname=onyx ",
        "netcfg/get_domain=vagrantup.com ",
        "osnova-license/license=true ",
        "debconf/frontend=noninteractive ",
        "<enter>"
    ]
}

source "qemu" "onyx" {
    iso_url = var.onyx_iso_url
    iso_checksum = var.onyx_iso_checksum
    shutdown_command = "echo 'password' | sudo -S /sbin/shutdown -hP now"
    disk_size = "30000M"
    memory = 5120
    format = "qcow2"
    accelerator = "kvm"
    http_content = {
        "/pressed.cfg" = templatefile("${path.root}/onyx.pkrtpl", {tasks = "ssh-server"})
    }
    ssh_username = "vagrant"
    ssh_password = "password"
    ssh_timeout = "25m"
    vm_name = "${source.name}"
    net_device = "virtio-net"
    disk_interface = "virtio"
    boot_wait = "5s"
    boot_command = "${var.boot_command}"
}

source "qemu" "onyx-kde" {
    iso_url = var.onyx_iso_url
    iso_checksum = var.onyx_iso_checksum
    shutdown_command = "echo 'password' | sudo -S /sbin/shutdown -hP now"
    disk_size = "30000M"
    memory = 5120
    format = "qcow2"
    accelerator = "kvm"
    http_content = {
        "/pressed.cfg" = templatefile("${path.root}/onyx.pkrtpl", {tasks = "ssh-server, osnova-kde"})
    }
    ssh_username = "vagrant"
    ssh_password = "password"
    ssh_timeout = "55m"
    vm_name = "${source.name}"
    net_device = "virtio-net"
    disk_interface = "virtio"
    boot_wait = "5s"
    boot_command = "${var.boot_command}"
}

build {
    sources = ["source.qemu.onyx", "source.qemu.onyx-kde"]
    provisioner "shell" {
        scripts = [
            "${path.root}/../debian/scripts/networking.sh",
            "${path.root}/../debian/scripts/ru-locale.sh",
            "${path.root}/../debian/scripts/lab50-key.sh",
            "${path.root}/scripts/cleanup.sh",
            "${path.root}/scripts/sources.sh",
            "${path.root}/../common/vagrant.sh",
            "${path.root}/../common/love.sh",
            "${path.root}/../common/machine-id-and-random-seed.sh",
            "${path.root}/../common/logs-and-cache.sh",
            "${path.root}/../common/minimize.sh"
        ]
    }
    post-processor "vagrant" {
        output = "${source.name}.box"
        vagrantfile_template = "files/Vagrantfile"
    }
}
