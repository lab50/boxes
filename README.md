Шаблоны для сборки образов Российских ОС от Лаборатории 50
==========================================================

Набор шаблонов Packer для сборки образов Российских ОС.
Образы предназначены для использования c системой Vagrant.

# Образы на rudev.io

- `astralinux/smolensk-1.6` — Astra Linux SE 1.6 с уровнем защищённости «Смоленск»
- `astralinux/smolensk-1.7` — Astra Linux SE 1.7 с уровнем защищённости «Смоленск»
- `astralinux/smolensk-fly-1.7` — Astra Linux SE 1.7 с уровнем защищённости «Смоленск» и рабочим столом Fly.
- `astralinux/orel-1.7` — Astra Linux SE 1.7 с уровнем защищённости «Орёл»
- `basealt/aronia` — Альт Starterkit 10 (сервер с systemd)
- `basealt/aronia-server` — Альт Сервер 10
- `basealt/aronia-workstation` — Альт Рабочая станция 10
- `basealt/aronia-simply` — Simply Linux на платформе 10 (Aronia)
- `lab50/bullseye`
- `lab50/bullseye-kde`
- `lab50/fedora35`
- `lab50/fedora35-kde`
- `lab50/fedora36`
- `lab50/fedora36-kde`
- `lab50/gosjava8` — ГосJava 8 на Debian Bullseye
- `lab50/gosjava11` — ГосJava 11 на Debian Bullseye
- `nppkt/onyx`
- `nppkt/onyx-kde`
- `redsoft/murom` — РЕД ОС Сервер
- `redsoft/murom-mate` — РЕД ОС Рабочая станция
- `rosa/2021.1-chrome-kde` — РОСА «ХРОМ» Рабочая станция на платформе 2021.1
- `rosa/2021.1-fresh` — ROSA Fresh 12
- `rosa/cobalt` — РОСА «КОБАЛЬТ» Сервер

# Использование готовых образов с VirtualBox

To be continued...

# Использование готовых образов с локальным libvirt

Дано:
- ОС Debian Bullseye;
- тестировалось с [libvirt](https://libvirt.org/) 7.0.0-3 и [Vagrant](https://www.vagrantup.com/) 2.4.0;
- образы лежат на `http://delta.lab50`.

Приступаем:
1. Установить libvirt:
   ```
   # apt-get -y install libvirt-daemon-system libvirt-clients libvirt-dev virt-manager qemu-system-x86-64
   ```
1. Добавить себя в группу `libvirt`:
   ```
   $ sudo usermod -aG libvirt $USER
   ```
   Чтобы изменения вступили в силу, надо перелогиниться.
1. Создать пул default в `/var/lib/libvirt/images`:
   ```
   $ virsh -c 'qemu:///system' pool-define-as default dir - - - - /var/lib/libvirt/images
   $ virsh -c 'qemu:///system' pool-start default
   $ virsh -c 'qemu:///system' pool-autostart default
   ```
1. Установить свежий Vagrant — https://www.vagrantup.com/downloads (может потребоваться VPN).
   Вот прямые ссылки на [vagrant_2.4.0-1_amd64.deb](https://releases.hashicorp.com/vagrant/2.4.0/vagrant_2.4.0-1_amd64.deb) и [vagrant-2.4.0-1.x86_64.rpm](https://releases.hashicorp.com/vagrant/2.4.0/vagrant-2.4.0-1.x86_64.rpm).
1. Уставить плагин Vagrant'а для работы с libvirt (может потребоваться VPN):
   ```
   $ vagrant plugin install vagrant-libvirt
   ```
   Если нужен SSHFS:
   ```
   $ vagrant plugin install vagrant-sshfs
   ```
   В качестве альтернативы можно грузить плагины с rubygems.org:
   ```
   $ export VAGRANT_ALLOW_PLUGIN_SOURCE_ERRORS=1
   $ vagrant plugin install --plugin-clean-sources --plugin-source https://rubygems.org vagrant-libvirt
   $ vagrant plugin install --plugin-clean-sources --plugin-source https://rubygems.org vagrant-sshfs
   ```
1. Создать `Vagrantfile` с одной ВМ на основе образа `fedora/fedora36`:
   ```ruby
   Vagrant.configure("2") do |config|
       # Общие настройки.
       config.vm.synced_folder ".", "/vagrant", disabled: true
       config.vm.provider :libvirt do |libvirt|
           libvirt.default_prefix = ""
       end
       # Настойки конкретной ВМ.
       config.vm.define "fedorka", default: true do |node|
           node.vm.hostname = "fedorka"
           node.vm.box = "fedora/fedora36"
           node.vm.box_url = "http://delta.lab50/vagrant/fedora/fedora36"
       end
   end
   ```
1. Создать ВМ:
   ```
   $ vagrant up
   ```
1. Теперь можно открыть `virt-manager`, там уже должно быть соединение, настроенное на `qemu:///system`.
   
   ![](img/virt-manager.png)

# Использование готовых образов с сервером libvirt

Дано всё то же, что и в пред. пункте, плюс:
- имя пользователя одно и то же на сервере и на локальной ЭВМ.

Настраиваем сервер libvirt на TCP-порте (для простоты сервер будет на `localhost`):
1. Установить libvirt:
   ```
   # apt-get -y install libvirt-daemon-system libvirt-clients qemu-system-x86-64
   ```
1. Механизм активации сокетов через systemd толком не работает с TCP, поэтому отключаем:
   ```
   # systemctl mask libvirtd.socket libvirtd-ro.socket libvirtd-admin.socket
   # systemctl mask libvirtd-tls.socket libvirtd-tcp.socket
   ```
1. Настроить и перезапустить libvirt:
   ```
   # sed -i '/^LIBVIRTD_ARGS=/c LIBVIRTD_ARGS="--listen"' /etc/default/libvirtd
   # sed -i '/^#listen_tcp = 1/s/#//' /etc/libvirt/libvirtd.conf
   # sed -i '/^#listen_tls = 0/s/#//' /etc/libvirt/libvirtd.conf
   # sed -i '/^#listen_addr =/c listen_addr = "0.0.0.0"' /etc/libvirt/libvirtd.conf
   # sed -i '/^#auth_tcp =/c auth_tcp = "none"' /etc/libvirt/libvirtd.conf
   # systemctl restart libvirtd.service
   ```
1. Создать пул default в `/var/lib/libvirt/images`:
   ```
   $ virsh -c 'qemu+tcp://localhost/system' pool-define-as default dir - - - - /var/lib/libvirt/images
   $ virsh -c 'qemu+tcp://localhost/system' pool-start default
   $ virsh -c 'qemu+tcp://localhost/system' pool-autostart default
   ```

Настраиваем клиентскую ЭВМ:
1. Установить клиенты libvirt:
   ```
   # apt-get -y install libvirt0 libvirt-dev virt-manager
   ```
1. Установить свежий Vagrant и соответствующие плагины (см. выше).
1. Создать файл `~/.ssh/id_rsa`, если его ещё нет:
   ```
   $ ssh-keygen
   ```
   Настроить SSH-доступ на сервер без пароля, если таковой ещё не настроен:
   ```
   $ ssh-copy-id $USER@localhost
   ```
1. Создать `Vagrantfile` с одной ВМ на основе образа `fedora/fedora36`:
   ```ruby
   Vagrant.configure("2") do |config|
       # Общие настройки.
       login = ENV["USER"]
       config.vm.synced_folder ".", "/vagrant", disabled: true
       config.vm.provider :libvirt do |libvirt|
           libvirt.connect_via_ssh = true
           libvirt.id_ssh_key_file = "/home/#{login}/.ssh/id_rsa"
           libvirt.host = "localhost"
           libvirt.username = login
           libvirt.uri = "qemu+tcp://localhost/system"
           libvirt.graphics_ip = "0.0.0.0"
           libvirt.default_prefix = ""
       end
       # Настойки конкретной ВМ.
       config.vm.define "fedorka", default: true do |node|
           node.vm.hostname = "fedorka"
           node.vm.box = "fedora/fedora36"
           node.vm.box_url = "http://delta.lab50/vagrant/fedora/fedora36"
       end
   end
   ```
1. Создать ВМ:
   ```
   $ vagrant up
   ```
1. Теперь можно создать в `virt-manager` новое соединение: `qemu+tcp://localhost/system`.

# Сборка box'ов

Зависимости для сборки:
- [Packer](https://www.packer.io/);
- [KVM](https://www.linux-kvm.org/page/Main_Page).

В нынешние непростые времена для загрузки Packer может потребоваться VPN.
Вот прямая ссылка на [packer_1.8.1-1_amd64.deb](https://apt.releases.hashicorp.com/pool/amd64/main/packer_1.8.1-1_amd64.deb).

Запустить сборку можно при помощи `Makefile` или вызвав `packer` вручную.
На примере Debian:
```
$ make bullseye
```
или:
```
$ cd packer_templates/debian
$ packer build -only qemu.bullseye bullseye.pkr.hcl
```

Запустить сборку с заданными переменными:
```
$ packer build -only qemu.bullseye \
    -var 'iso_url=./debian-11.3.0-amd64-netinst.iso' \
    -var 'iso_checksum=md5:e7a5a4fc5804ae65f7487e68422368ad' \
    bullseye.pkr.hcl
```
Переменные объявляются в файле `pkr.hcl`.

## ALT Linux

Поддерживается сборка версий:
1. [Альт Starterkit](https://www.altlinux.org/Starterkits) (сервер с systemd).
1. [Альт Сервер 10](https://www.altlinux.org/Альт_Сервер_10).
1. [Альт Рабочая станция 10](https://www.altlinux.org/Альт_Рабочая_станция_10).
1. [Simply Linux 10](https://www.altlinux.org/Simply_Linux_10).

Собрать образы:
```
$ make aronia-starterkit
$ make aronia-server
$ make aronia-workstation
$ make aronia-simply
```
или:
```
$ cd packer_templates/basealt
$ packer build -only qemu.starterkit aronia.pkr.hcl
$ packer build -only qemu.server aronia.pkr.hcl
$ packer build -only qemu.workstation aronia.pkr.hcl
$ packer build -only qemu.simply aronia.pkr.hcl
```

Переменные:
- `starterkit_iso_url` и `starterkit_iso_checksum` — установочный образ Альт Starterkit и его контрольная сумма;
- `server_iso_url` и `server_iso_checksum` — установочный образ Альт Сервер 10 и его контрольная сумма;
- `workstation_iso_url` и `workstation_iso_checksum` — установочный образ Альт Рабочая станция 10 и его контрольная сумма;
- `simply_iso_url` и `simply_iso_checksum` — установочный образ Simply Linux 10 и его контрольная сумма.

## Astra Linux Special Edition

Поддерживается сборка версий:
1. Astra Linux SE 1.6 с уровнем защищённости «Смоленск».
1. Astra Linux SE 1.7 с уровнем защищённости «Смоленск».
1. Astra Linux SE 1.7 с уровнем защищённости «Смоленск» и рабочим столом Fly.
1. Astra Linux SE 1.7 с уровнем защищённости «Орёл».

Необходимо скачать установочный образ в каталог, из которого будет производиться
сборка, либо указать образ при помощи переменной.

Обновления безопасности устанавливаются из официального репозитория.

Собрать образы:
```
$ make smolensk-1.6
$ make smolensk-1.7
$ make smolensk-fly-1.7
$ make orel-1.7
```
или:
```
$ cd packer_templates/astralinux
$ packer build -only qemu.smolensk astra-1.6.pkr.hcl
$ packer build -only qemu.smolensk astra-1.7.pkr.hcl
$ packer build -only qemu.smolensk-fly astra-1.7.pkr.hcl
$ packer build -only qemu.orel astra-1.7.pkr.hcl
```

Переменные:
- `smolensk_1_6_iso_url` и `smolensk_1_6_iso_checksum` — установочный образ
  Astra Linux SE 1.6 (по умолчанию `smolensk-1.6-20.06.2018_15.56.iso`)
  и его контрольная сумма.
- `smolensk_1_7_iso_url` и `smolensk_1_7_iso_checksum` — установочный образ
  Astra Linux SE 1.7 (по умолчанию `smolensk-1.7.0-11.06.2021_12.40.iso`)
  и его контрольная сумма.

## Debian

Поддерживается сборка версий:
1. Debian Bullseye.
1. Debian Bullseye с KDE.

Собрать образы:
```
$ make bullseye
$ make bullseye-kde
```
или:
```
$ cd packer_templates/debian
$ packer build -only qemu.bullseye bullseye.pkr.hcl
$ packer build -only qemu.bullseye-kde bullseye.pkr.hcl
```

Переменные:
- `iso_url` и `iso_checksum` — установочный образ Debian Bullseye и его контрольная сумма.

## РОСА

Поддерживается сборка версий:
1. РОСА «ХРОМ» Рабочая станция.
1. ROSA Fresh 12.
1. РОСА «КОБАЛЬТ» Сервер.

Собрать образы:
```
$ make chrome-kde
$ make fresh
$ make cobalt
```
или
```
$ cd packer_templates/rosa
$ packer build -only qemu.chrome-kde rosa.pkr.hcl
$ packer build -only qemu.fresh rosa.pkr.hcl
$ packer build -only qemu.cobalt rosa.pkr.hcl
```

Переменные:
- `chrome_iso_url` и `chrome_iso_checksum` — установочный образ РОСА «ХРОМ»
  и его контрольная сумма.
- `fresh_iso_url` и `fresh_iso_checksum` — установочный образ ROSA Fresh 12
  и его контрольная сумма.
- `cobalt_iso_url` и `cobalt_iso_checksum` — установочный образ РОСА «КОБАЛЬТ»
  (по умолчанию `ROSA-COBALT.iso`) и его контрольная сумма.

## Тестирование собранных образов

1. Зарегистрировать собранный образ в Vagrant:
   ```
   $ vagrant box add --name test/bullseye-kde bullseye-kde.box
   ```
1. Настроить окружение как описано выше, в `Vagrantfile` убрать `box_url` и изменить `box`:
   ```ruby
   node.vm.box = "test/bullseye-kde"
   ```

В корне проекта находится `Vagrantfile` для тестирования,
который сам выполняет `vagrant box add` перед созданием ВМ.

# Похожие проекты

- [https://github.com/boxcutter](https://github.com/boxcutter)
- [https://github.com/chef/bento](https://github.com/chef/bento)

# Лицензия

Распространяется под лицензией GNU General Public License v3.0.
Полный текст лицензии в файле LICENSE.

# Контакты

Репозиторий проекта: https://gitlab.com/lab50/rudev.io/boxes

Разработка поддерживается компанией [Лаборатория 50](https://lab50.net).
