
def pytest_addoption(parser):
    parser.addoption('--file', action='store')
    parser.addoption('--pool',
                     action='store',
                     default='default',
                     help='libvirt pool')
    parser.addoption('--connection',
                     action='store',
                     default='qemu:///system',
                     help='libvirt server URI')
