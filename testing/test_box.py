import pytest 
import sh
import uuid
import shutil
import pathlib
from tempfile import gettempdir
from string import Template


@pytest.fixture(scope="session")
def boxfile(pytestconfig):
    file_arg = pytestconfig.getoption('file')

    if file_arg is None:
        raise ValueError('--file not set.')

    boxfile = pathlib.Path(file_arg)

    if not boxfile.is_file():
        raise FileNotFoundError("Box file not found.")

    return boxfile 


@pytest.fixture(scope="session")
def pool(pytestconfig):
    return pytestconfig.getoption('pool')


@pytest.fixture(scope="session")
def connection(pytestconfig):
    return pytestconfig.getoption('connection')


@pytest.fixture(scope="session")
def test_uuid():
    return uuid.uuid4();


@pytest.fixture(scope="session")
def box(test_uuid, connection, pool, boxfile):
    box = "test-"  + test_uuid.hex
    sh.vagrant('box', 'add', '--name', box, boxfile, _fg=True)
    yield box
    sh.vagrant('box', 'remove', box, _fg=True)
    sh.virsh('-c', connection, 'vol-delete', '--pool', pool, box + '_vagrant_box_image_0_box.img')


@pytest.fixture(scope="session")
def tmpdir(test_uuid):
    directory = "test-vagrant-box-" + test_uuid.hex
    path = pathlib.Path(gettempdir(), directory)
    path.mkdir(parents=True)
    yield path
    shutil.rmtree(path)


@pytest.fixture(scope="session")
def vagrantfile(tmpdir, connection, pool, box):
    template = Template(pathlib.Path('Vagrantfile.template').read_text())
    path = pathlib.Path(tmpdir, "Vagrantfile")
    path.write_text(template.substitute(pool=pool, box=box, uri=connection))
    yield path


@pytest.mark.usefixtures("box") 
@pytest.mark.usefixtures("vagrantfile") 
def test_up(tmpdir):
    vagrant = sh.Command('vagrant').bake(_cwd=tmpdir, _fg=True)

    vagrant('up')
    vagrant('ssh', '-c', 'id')
    vagrant('destroy', '-f')
